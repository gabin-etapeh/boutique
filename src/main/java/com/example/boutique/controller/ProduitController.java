package com.example.boutique.controller;

import com.example.boutique.entity.Categorie;
import com.example.boutique.entity.Produit;
import com.example.boutique.repository.CategorieRepository;
import com.example.boutique.repository.ProduitRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class ProduitController {

    private final ProduitRepository produitRepository;
    private final CategorieRepository categorieRepository;

    public ProduitController(ProduitRepository produitRepository,
                             CategorieRepository categorieRepository){
        this.produitRepository = produitRepository;
        this.categorieRepository = categorieRepository;
    }

    // Une API (endpoint) pour créer un produit [POST]
    @PostMapping("/produit")
    public ResponseEntity<Produit> addProduit (@RequestBody Produit produit,
                                               @RequestParam("idCategorie") Long idCategorie){

        Categorie categorie = categorieRepository.findById(idCategorie).orElse(null);
        produit.setCategorie(categorie);
        Produit nouveauProduit = produitRepository.save(produit);
        return new ResponseEntity<>(nouveauProduit, HttpStatus.CREATED);
    }

    // Une API pour récupérer un Produit suivant son identifiant [GET]
    @GetMapping("/produit/{id}")
    public ResponseEntity<Produit> getProduit (@PathVariable long id){
        Produit produit = produitRepository.findById(id).orElse(null);
        return new ResponseEntity<>(produit, HttpStatus.OK);
    }

    // Une API pour modifier un Produit [PUT] ou [PATCH]
    @PutMapping("/produit/{idProd}")
    public ResponseEntity<?> updateProduit (@PathVariable long id, @RequestBody Produit nouveauProduit){

        Optional<Produit> ancienProduitOptional = produitRepository.findById(id);
        if(ancienProduitOptional.isPresent()){

            Produit ancienProduit = ancienProduitOptional.get();
            ancienProduit.setNom(nouveauProduit.getNom());
            ancienProduit.setPrix(nouveauProduit.getPrix());
            ancienProduit.setQuantite(nouveauProduit.getQuantite());
            ancienProduit.setDescription(nouveauProduit.getDescription());

            produitRepository.save(ancienProduit);

            System.out.println("Mise à jour d'un Produit ...");
            return new ResponseEntity<>(ancienProduit, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Aucun produit à modifier", HttpStatus.NOT_FOUND);
        }
    }

    // Une API pour supprimer un Produit connaissant son identifiant [DELETE]
    @DeleteMapping("/produit/{id}")
    public ResponseEntity<Produit> deleteProduit (@PathVariable long id){
        produitRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
