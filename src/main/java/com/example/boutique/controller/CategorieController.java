package com.example.boutique.controller;

import com.example.boutique.entity.Categorie;
import com.example.boutique.entity.Produit;
import com.example.boutique.repository.CategorieRepository;
import com.example.boutique.repository.ProduitRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CategorieController {
    private final CategorieRepository categorieRepository;

    public CategorieController(CategorieRepository categorieRepository){
        this.categorieRepository = categorieRepository;
    }

    // Une API (endpoint) pour créer une categorie [POST]
    @PostMapping("/categorie")
    public ResponseEntity<Categorie> addCategorie (@RequestBody Categorie categorie){
        Categorie nouvelleCategorie = categorieRepository.save(categorie);
        return new ResponseEntity<>(nouvelleCategorie, HttpStatus.CREATED);
    }

    // Une API pour récupérer un Categorie suivant son identifiant [GET]
    @GetMapping("/categorie/{id}")
    public ResponseEntity<Categorie> getCategorie (@PathVariable long id){
        Categorie categorie = categorieRepository.findById(id).orElse(null); // Optional<Categorie>
        return new ResponseEntity<>(categorie, HttpStatus.OK);
    }

    // Une API pour modifier un Categorie [PUT] ou [PATCH]
    @PutMapping("/categorie/{id}")
    public ResponseEntity<Produit> updateCategorie (@PathVariable long id, @RequestBody Categorie nouvelleCategorie){
        // TODO
        System.out.println("Mise à jour d'un Categorie ...");
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    // Une API pour supprimer un Categorie connaissant son identifiant [DELETE]
    @DeleteMapping("/categorie/{id}")
    public ResponseEntity<Categorie> deleteCategorie (@PathVariable long id){
        categorieRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
