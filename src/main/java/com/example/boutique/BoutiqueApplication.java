package com.example.boutique;

import com.example.boutique.entity.Produit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoutiqueApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoutiqueApplication.class, args);

		Produit produit = Produit.builder()
				.nom("Savon")
				.quantite(45)
				.description("Descrition xxxxxxxxxxxxxxxxxxx")
				.build();
	}

}
